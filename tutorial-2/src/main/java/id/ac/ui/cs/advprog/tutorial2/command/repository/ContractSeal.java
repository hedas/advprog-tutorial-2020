package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.BlankSpell;
import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {

    private Map<String, Spell> spells;
    private Spell latestSpell;

    public ContractSeal() {
        this.spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        this.spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // TODO: Complete Me
        Spell currSpell = this.spells.get(spellName);
        currSpell.cast();
        this.latestSpell = currSpell;
    }

    public void undoSpell() {
        // TODO: Complete Me
        this.latestSpell.undo();
        this.latestSpell = null;
    }

    public Collection<Spell> getSpells() { 
        return spells.values(); 
    }
}
