package id.ac.ui.cs.advprog.tutorial5;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
@SpringBootTest
class Tutorial5ApplicationTests {

	@Test
	void contextLoads() {
		assertEquals("1", "1");
	}

}
