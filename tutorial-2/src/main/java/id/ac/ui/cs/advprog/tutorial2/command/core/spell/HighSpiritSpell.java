package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpiritState;

public abstract class HighSpiritSpell implements Spell {
	protected HighSpirit spirit;

    public HighSpiritSpell(HighSpirit spirit) {
        // TODO: Complete Me
        this.spirit = spirit;
    }

    @Override
    public void undo() {
        // TODO: Complete Me
        HighSpiritState prevState = spirit.getPrevState();
        if (prevState == HighSpiritState.ATTACK) {
            spirit.attackStance();
        }
        else if (prevState == HighSpiritState.DEFEND) {
            this.spirit.defenseStance();
        }
        else if (prevState == HighSpiritState.STEALTH) {
            this.spirit.stealthStance();;
        }
        else if (prevState == HighSpiritState.SEALED) {
            this.spirit.seal();
        }
    }
}
