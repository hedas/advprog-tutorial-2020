package id.ac.ui.cs.advprog.tutorial5.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository; 
import id.ac.ui.cs.advprog.tutorial5.core.Soul;

@Service
public class SoulServiceImpl implements SoulService {

    @Autowired
    private SoulRepository soulRepository;

    @Override
    public List<Soul> findAll() {
        return this.soulRepository.findAll();
    }

    @Override
    public Optional<Soul> findSoul(Long id) {
        return this.soulRepository.findById(id);
    }

    @Override
    public void erase(Long id) {
        this.soulRepository.deleteById(id);

    }

    @Override
    public Soul rewrite(Long id, Soul soul) {
        this.erase(id);
        return this.register(soul);
    }

    @Override
    public Soul register(Soul soul) {
        return this.soulRepository.save(soul);
    }
}
