package id.ac.ui.cs.advprog.tutorial5.controller;

import java.net.URI;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import id.ac.ui.cs.advprog.tutorial5.service.SoulService;

// TODO: Import apapun yang anda perlukan agar controller ini berjalan
@RestController
@RequestMapping(path = "/soul")
public class SoulController {
    
    private SoulService soulService;

    @Autowired
    public SoulController(SoulService soulService) {
        this.soulService = soulService;
    }


    @GetMapping
    public ResponseEntity<List<Soul>> findAll() {
        return new ResponseEntity<List<Soul>>(this.soulService.findAll(), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody Soul soul) {
        soul.setId(0);
        Soul newSoul = soulService.register(soul);
        return ResponseEntity.created(URI.create("/soul/" + newSoul.getId())).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Soul> findById(@PathVariable Long id) {
        Soul soul;
        if(this.soulService.findSoul(id).isPresent()){
            soul = this.soulService.findSoul(id).get();
        }else{
            soul = null;
        }
        return new ResponseEntity<Soul>(soul, HttpStatus.OK);
    }
	
	@PutMapping("/{id}")
    public ResponseEntity<Soul> update(@PathVariable Long id, @RequestBody Soul soul) {
        Soul newSoul = this.soulService.rewrite(id, soul);
        return new ResponseEntity<Soul>(newSoul, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        this.soulService.erase(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
